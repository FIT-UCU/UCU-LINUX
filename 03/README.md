# Filesystem & LVM excercises

Una VM con dos discos sin utilizar

## Ejercicios

* Identificación de carpetas en el filesystem
* Particiones
* Formatos
* LVM
* Cifrado disco
* Tipos de archivos
