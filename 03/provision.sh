#!/bin/bash
echo "Vagrant provision at $(date +%F-%H.%M.%S)" > /tmp/created
apt-get update
DEBIAN_FRONTEND=noninteractive apt-get install -y \
	xfsprogs reiserfsprogs btrfs-progs \
	vim encfs fuse cryptsetup lvm2

