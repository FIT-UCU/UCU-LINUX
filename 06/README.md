# Containers & Docker excercises

Instalación básica con docker y docker-compose

Tema: Procesos y monitor de sistema

## Ejercicios

### Docker - Guía del Usuario

[Guia de usuario](https://pilasguru.gitlab.io/Docker-GuiaParaElUsuario/)

## Docker

### CTop

```
docker run --rm -ti \
    --name=ctop \
    -v /var/run/docker.sock:/var/run/docker.sock \
    quay.io/vektorlab/ctop:latest
```

<https://github.com/bcicen/ctop>

### Lazydocker

```
docker run --rm -it -v \
    /var/run/docker.sock:/var/run/docker.sock \
    -v /yourpath:/.config/jesseduffield/lazydocker \
    lazyteam/lazydocker
```

### Glances

```
docker run --rm -v /var/run/docker.sock:/var/run/docker.sock:ro \
    --pid host --network host -it \
    nicolargo/glances:3.2.0-full
```

### Hello app

```
docker run --rm -d -p 8080:8080 gcr.io/google-samples/hello-app:2.0
````
