# UCU-LINUX
![UCU 30](http://www.ucu.edu.uy/sites/all/themes/univer/logo.png)
## Facultad de Ingeniería y Tecnologías
### Materia: Instalación Configuración y Administración de Linux

**Profesor: Rodolfo Pilas**

Este conjunto de archivos son necesarios para levantar la infraestructura (virtual) requerida para seguir las diferentes etapas del curso.

### Requisitos previos

En términos generales, se requiere disponer de estos componentes instalados localmente:

1. Cliente de repositrio GIT: git o Git-bash
2. Sistema de Virtualización: Oracle Virtualbox 
3. Software de provision y aprovisionamiento de instancias virtuales: Vagrant
4. Cliente para acceso Secure Shell: OpenSSH o Git-bash
5. Algunas imágenes para provisionar máquinas virtuales: vagrant box

Sobre el hardware es preferble disponer de un procesador 64bits con las extensiones de virtualización activadas y al menos 1GB de RAM. 

*NOTA:* En caso de no disponer de un procesador con extensiones de virtualizacón activas, el virtualizador (Virtualbox) solo levantará máquinas virtuales de 32bits (aunque el procesador real sea de 64bits), por lo que las imágenes (boxes) deben ser cambiadas por equivalentes en 32bits.

#### Software para Windows

* Virtualbox [https://www.virtualbox.org/wiki/Downloads](https://www.virtualbox.org/wiki/Downloads)
	* VirtualBox platform packages
	* VirtualBox VirtualBox Extension Pack
* Git-Bash [https://git-for-windows.github.io/](https://git-for-windows.github.io/)
* Vagrant [https://www.vagrantup.com/downloads.html](https://www.vagrantup.com/downloads.html)

Video explicativo en Youtube: 
[![Vagrant on Windows](https://i.ytimg.com/vi/Jkf5g7L9dSE/hqdefault.jpg)](https://www.youtube.com/watch?v=Jkf5g7L9dSE)

#### Software para Linux

* Git (del repositorio de la distribución)
* Virtualbox [https://www.virtualbox.org/wiki/Downloads](https://www.virtualbox.org/wiki/Downloads)
	* VirtualBox platform packages
	* VirtualBox VirtualBox Extension Pack
* Vagrant [https://www.vagrantup.com/downloads.html](https://www.vagrantup.com/downloads.html)

(se sugiere versiones _descargadas de la página_, pues paquetes suelen tener versiones más antiguas)

Video explicativo en Youtube:
[![Vagrant on Ubuntu](https://i.ytimg.com/vi/VI5m1UyNBiE/hqdefault.jpg)](https://www.youtube.com/watch?v=VI5m1UyNBiE)

#### Sobre sistema MacOSX

* Virtualbox [https://www.virtualbox.org/wiki/Downloads](https://www.virtualbox.org/wiki/Downloads)
	* VirtualBox platform packages
	* VirtualBox VirtualBox Extension Pack
* Vagrant [https://www.vagrantup.com/downloads.html](https://www.vagrantup.com/downloads.html)

(se sugiere versiones descargadas de la página, en lugar de gestor de paquetes)

#### Imagenes máquinas virtuales (boxes)

Varias imágenes (boxes) pueden ser descargadas del sitio
[http://www.vagrantbox.es/](http://www.vagrantbox.es/)

#### Plugins

Vagrant soporta plugins para aumentar su funcionalidad, durante el curso se utilizaran varios plugins.

### Instalación

1. Instalar software previo.
2. (en Windows): Agregar al PATH el directorio *bin* de Git-bash, semejante a: C:\Program Files\Git-Bash\bin (confirmar en cada instalación)
3. Cargar al repositorio de boxes algunas imágenes de máquinas virtuales:

* Descargar imágenes (.box) de [https://app.vagrantup.com/boxes/search](https://app.vagrantup.com/boxes/search)
* Para cada box descargada ejecutar: 

```
vagrant box add [archivo.box]
    
vagrant box add debian-8-jessie-rc2-x64-slim.box
```

* Verificar que quedaron cargadas las boxes con el comando:

```
vagrant box list
```

### Comandos para ejecutar en clase:

1. Clonar este repositorio

Ejecutando:

```
git clone https://gitlab.com/pilasguru/UCU-LINUX.git 
```

2. Levantar máquinas

```
cd UCU-LINUX/00
vagrant up
```

La primera vez descargará todos los paquetes necesarios para la provisión de las VM.

### Notas

La estructura virtual que queda formada (*en algunos ejercicios*)  es:

![Network](https://gitlab.com/pilasguru/UCU-LINUX/raw/master/docs/UCU-LINUX.png)

Estas máquinas virtuales que son levantadas

* router
* lamp

tienen software que se provee automáticamente y el que no puede ser proveído se dejan disponibles distintos archivos para utilizar en el curso.

