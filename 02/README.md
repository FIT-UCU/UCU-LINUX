# Multiple VM y sub-net

Tres VM con red privada

## Ejercicios

### OpenSSH

Cada VM dispone del usuario `ucu` con la password `pass` por defecto

- usos de ssh
- certificados y validacion (aprovisionamiento de certificados)
- configuraciones de seguridad
  - servidor
  - cliente
- usos avanzados
  - pipeline
- otras herramientas de SSH

## Vagrantfile

### Provider

Tres VM con una sub-red privada

```
host ->  10.0.2.15   srv01   10.0.0.11    running (virtualbox)
host ->  10.0.2.15   srv02   10.0.0.12    running (virtualbox)
host ->  10.0.2.15   srv03   10.0.0.13    running (virtualbox)
```

### Provision

- Crea archivo /tmp/created
- Instala comando `git`
- Crea usuario `ucu` 



