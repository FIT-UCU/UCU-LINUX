# InstantBOX

Esta configuración permite levantar una VM que lanza distintas versiones
de Linux en contenedores Docker por un tiempo limitado.  

Todo es 100% accesible por web: [http://localhost:8888](http://localhost:8888)

La herramienta utilziada es el proyecto [InstantBOX](https://github.com/instantbox/instantbox) 
distribuído bajo [Licencia MIT](https://github.com/instantbox/instantbox/blob/master/LICENSE).

## Ejercicios

### Package manager

- Paquetes instalados
- Actualizacion de caches
- Repositorios
- Búsqueda de software disponible
- Instalación de nuevos paquetes
- Actualización

## Vagrantfile

### Provider

Una VM con la siguiente configuración:

```
BOX_RAM_MB = "4092"
BOX_CPU_COUNT = "2"
BOX_BASE = "ubuntu/focal64"
BOX_NAME = "instantbox"
EXTERNAL_PORT = "8888"
```

### Provision

- Docker 
- docker-compose
- Proyecto InstantBOX




