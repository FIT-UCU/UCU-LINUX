# Instalación de testing

En esta carpeta se dispone de un `Vagrantfile` básico para probar que vagrant y el sistema de virtulización funcionan correctamente.

Descargará y dejará disponible la imagen `maier/alpine-3.4-x86_64` para futuros usos.

## Ejecutar

```
vagrant up
```

como se muestra en el siguiente video:

[![asciicast](https://gitlab.com/pilasguru/UCU-LINUX/raw/master/docs/107964.png)](https://asciinema.org/a/107964)

## Revisar instalación

* Conectarse a `http://localhost:8080`
* Entrar por _ssh_ a la máquina virtual ejecutando `vagrant ssh`
* Verificar descarga de _box_ con `vagrant box list`
* Gestión de máquina virtual con los siguientes comandos:
	* `vagrant status`
	* `vagrant halt` 
	* `vagrant stop`
	* `vagrant up`
	* `vagrant destroy`

## Ver además

* [Vagrant cheat-sheet](https://pi.lastr.us/doku.php?id=docs:virtualizacion:vagrant:vagrant_cheat_sheet)
* [Vagrant documentation](https://www.vagrantup.com/docs/index.html)
* [Vagrant Cloud](https://app.vagrantup.com/boxes/search) repositorio de _boxes_

## Libros recomendados

* [Vagrant cookbook](https://leanpub.com/vagrantcookbook)
* [Vagrant up and running](http://shop.oreilly.com/product/0636920026358.do)


